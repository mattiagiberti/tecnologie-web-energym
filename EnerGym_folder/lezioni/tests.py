from django.test import TestCase

# Create your tests here.
from django.urls import reverse
from django.utils import timezone

from gestione_palestra.models import GymUser, InsegnanteCorso, Corso
from lezioni.forms import LezioneForm
from lezioni.models import Lezione, Prenotazione


class TestLezione(TestCase):

    def setUp(self):
        self.corso = Corso.objects.create(nome="nomecorso")

        self.insegnante = GymUser.objects.create(
            email="trainer@trainer.com",
            first_name="personal",
            last_name="trainer",
            username="trainer",
            is_insegnante=True
        )

        self.insegnantecorso = InsegnanteCorso.objects.create(corso=self.corso, insegnante=self.insegnante)

        self.user1 = GymUser.objects.create(
            email="user1@user1.com",
            first_name="firstname1",
            last_name="lastname1",
            username="user1"
        )

        self.user2 = GymUser.objects.create(
            email="user2@user2.com",
            first_name="firstname2",
            last_name="lastname2",
            username="user2"
        )

    def test_crea_lezione_posti_valido(self):
        """
        form valido per la creazione di una classe, numero dei posti > 0,
        deve ritornare True
        """
        form = LezioneForm(user=self.insegnante, data={
            'insegnantecorso': self.insegnantecorso,
            'descrizione': 'descrizione',
            'posti_disponibili_tot': 10,
            'data': '2222-12-05 16:00'
        })
        self.assertEqual(form.is_valid(), True)

    def test_crea_lezione_posti_non_valido(self):
        """
            form non valido per la creazione di una classe, numero dei posti = 0,
            deve ritornare False
        """
        form = LezioneForm(user=self.insegnante.id, data={
            'insegnantecorso': self.insegnantecorso,
            'descrizione': 'descrizione',
            'posti_disponibili_tot': 0,
            'data': '2222-12-05 16:00'
        })
        self.assertEqual(form.is_valid(), False)

    def test_crea_lezione_posti_non_valido_numero_negativo(self):
        """
            form non valido per la creazione di una classe, numero dei posti negativo,
            deve ritornare False
        """
        form = LezioneForm(user=self.insegnante.id, data={
            'insegnantecorso': self.insegnantecorso,
            'descrizione': 'descrizione',
            'posti_disponibili_tot': -4,
            'data': '2222-12-05 16:00'
        })
        self.assertEqual(form.is_valid(), False)

    def test_update_lezione_posti_valido(self):
        """
            form valido per l'aggiornamento di una lezione,
            il nuovo numero dei posti totali disponibili è maggiore dei clienti già iscritti alla lezione,
            deve ritornare False
        """
        lezione = Lezione.objects.create(
            insegnantecorso=self.insegnantecorso,
            posti_disponibili_tot=2,                                    # 2 posti disponibili
            data='2222-12-05 16:00'
        )
        Prenotazione.objects.create(user=self.user1, lezione=lezione)   # user1 iscritto
        Prenotazione.objects.create(user=self.user2, lezione=lezione)   # user2 iscritto
        form = LezioneForm(user=self.insegnante.id, data={
            'insegnantecorso': self.insegnantecorso,
            'descrizione': 'descrizione',                               # nuovi posti disponibili 10
            'posti_disponibili_tot': 10,                                # quindi rimangono liberi 8 posti
            'data': '2222-12-05 16:00'
        }, instance=lezione)
        self.assertEqual(form.is_valid(), True)

    def test_update_lezione_posti_non_valido(self):
        """
            form non valido per l'aggiornamento di una lezione,
            il nuovo numero dei posti totali disponibili è minore dei clienti già iscritti alla lezione,
            deve ritornare False
        """
        lezione = Lezione.objects.create(
            insegnantecorso=self.insegnantecorso,
            posti_disponibili_tot=2,                                    # 2 posti disponibili
            data='2222-12-05 16:00'
        )
        Prenotazione.objects.create(user=self.user1, lezione=lezione)   # user1 iscritto
        Prenotazione.objects.create(user=self.user2, lezione=lezione)   # user2 iscritto
        form = LezioneForm(user=self.insegnante.id, data={
            'insegnantecorso': self.insegnantecorso,
            'descrizione': 'descrizione',
            'posti_disponibili_tot': 1,                                 # nuovi posti disponibili 1
            'data': '2222-12-05 16:00'                                  # quindi NON rimangono posti liberi
        }, instance=lezione)
        self.assertEqual(form.is_valid(), False)

    def test_lezione_posti_disponibili_non_terminati(self):
        """
            Verifico che la funzione return_posti_rimasti ritorni setti correttamente il campo posti_finiti
        """
        lezione = Lezione.objects.create(
            insegnantecorso=self.insegnantecorso,
            posti_disponibili_tot=2,                                    # 2 posti disponibili
            data='2222-12-05 16:00'
        )
        Prenotazione.objects.create(user=self.user1, lezione=lezione)   # user1 iscritto

        lezione.return_posti_rimasti()

        self.assertEqual(lezione.posti_finiti, False)

    def test_lezione_posti_disponibili_terminati(self):
        """
            Verifico che la funzione return_posti_rimasti ritorni setti correttamente il campo posti_finiti
        """
        lezione = Lezione.objects.create(
            insegnantecorso=self.insegnantecorso,
            posti_disponibili_tot=2,                                    # 2 posti disponibili
            data='2222-12-05 16:00'
        )
        Prenotazione.objects.create(user=self.user1, lezione=lezione)   # user1 iscritto
        Prenotazione.objects.create(user=self.user2, lezione=lezione)   # user2 iscritto

        lezione.return_posti_rimasti()

        self.assertEqual(lezione.posti_finiti, True)
