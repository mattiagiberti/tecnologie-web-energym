from django.db import models
from django.utils import timezone

from gestione_palestra.models import Corso, GymUser, InsegnanteCorso


class Lezione(models.Model):
    insegnantecorso = models.ForeignKey(InsegnanteCorso, on_delete=models.PROTECT, related_name='insegnantecorso',
                                        null=True)
    descrizione = models.TextField(max_length=400)
    posti_disponibili_tot = models.PositiveIntegerField()
    data = models.DateTimeField()
    posti_finiti = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'Lezioni'

    def __str__(self):
        return self.insegnantecorso.insegnante.last_name + ' - ' + self.insegnantecorso.corso.nome + ' - ' + str(
            self.data)

    def return_posti_rimasti (self):
        posti_rimasti = self.posti_disponibili_tot - self.return_posti_occupati()
        if posti_rimasti < 1:
            self.posti_finiti = True
        else:
            self.posti_finiti = False
        return posti_rimasti

    def return_posti_occupati (self):
        posti_occupati = 0
        list = Prenotazione.objects.filter(lezione_id=self.id).values_list('lezione_id', flat=True)
        for x in list:
            if x == self.id:
                posti_occupati += 1
        return posti_occupati

    def non_disponibile(self):
        return self.data < timezone.now()


class Prenotazione(models.Model):
    user = models.ForeignKey(GymUser, on_delete=models.CASCADE)
    lezione = models.ForeignKey(Lezione, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('user', 'lezione')
        verbose_name_plural = 'Prenotazioni'

    def __str__(self):
        return 'Lezione di {} del {} - prenotazione by [username]: {}'.format(self.lezione.insegnantecorso.corso.nome,
                                                                              self.lezione.data,
                                                                              self.user.username
                                                                              )
