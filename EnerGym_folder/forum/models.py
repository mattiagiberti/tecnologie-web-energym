from django.db import models

from gestione_palestra.models import GymUser


class Topic_lezione(models.Model):
    titolo = models.CharField(max_length=40)
    descrizione = models.TextField(max_length=999)
    gymuser_topic = models.ForeignKey(GymUser, on_delete=models.PROTECT, related_name='gymuser_topic')
    creato = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'Topic-Lezioni'
        ordering = ['creato']

    def __str__(self):
        return self.titolo


class Commento(models.Model):
    topic = models.ForeignKey(Topic_lezione, on_delete=models.CASCADE, related_name='topic')
    gymuser_lezione = models.ForeignKey(GymUser, on_delete=models.PROTECT, related_name='gymuser_lezione')
    creato = models.DateTimeField(auto_now_add=True)
    testo = models.TextField(max_length=300)

    class Meta:
        verbose_name_plural = 'Commenti'

    def __str__(self):
        return 'Comment {} by {} in topic {}'.format(self.id, self.gymuser_lezione, self.topic_id)
