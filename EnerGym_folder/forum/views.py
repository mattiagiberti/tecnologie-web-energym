from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.db.models import Q
from forum.forms import CommentoForm, TopicForm
from forum.models import Topic_lezione, Commento


@login_required()
def forum_homepage(request):

    topics = Topic_lezione.objects.all().order_by('-creato')
    query = request.GET.get('q')
    if query:
        topics = topics.filter(
            Q(titolo__icontains=query)|
            Q(descrizione__icontains=query)|
            Q(gymuser_topic__username__icontains=query)
        ).distinct()

    paginator = Paginator(topics, 5)
    page_request_var = "page"
    page = request.GET.get(page_request_var)

    try:
        topics = paginator.page(page)
    except PageNotAnInteger:
        topics = paginator.page(1)
    except EmptyPage:
        topics = paginator.page(paginator.num_pages)

    context = {
        'topics': topics,
        'page_request_var': page_request_var,
    }

    return render(request, 'forum/forum.html', context)


@login_required()
def topic(request, id):
    topic = Topic_lezione.objects.get(id=id)
    commenti = Commento.objects.filter(topic=topic)
    form = CommentoForm(request.POST or None)

    return render(request, 'forum/topic.html', {'topic': topic, 'commenti': commenti, 'form': form})


@login_required()
def save_commento(request, form, template_name, topic):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            nuovo_commento_form = form.save(commit=False)
            nuovo_commento_form.gymuser_lezione = request.user
            nuovo_commento_form.topic = topic
            nuovo_commento_form.save()
            data['form_is_valid'] = True
            context = {'form': nuovo_commento_form}
        else:
            data['form_is_valid'] = False
            context = {'form': form}
    else:
        context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@login_required
def crea_commento(request):

    if request.method == 'POST':
        topic_id = request.POST.get('id')
        topic = Topic_lezione.objects.get(id=topic_id)
        form = CommentoForm(data=request.POST)
    else:
        topic_id = request.GET.get('id')
        topic = Topic_lezione.objects.get(id=topic_id)
        form = CommentoForm()

    return save_commento(request, form, 'forum/crea_commento.html', topic)


@login_required
def update_commento(request, pk):

    commento = get_object_or_404(Commento, pk=pk)

    if request.method == 'POST':
        topic_id = request.POST.get('id')
        topic = Topic_lezione.objects.get(id=topic_id)
        form = CommentoForm(request.POST, instance=commento)
    else:
        topic_id = request.GET.get('id')
        topic = Topic_lezione.objects.get(id=topic_id)
        form = CommentoForm(instance=commento)

    return save_commento(request, form, 'forum/update_commento.html', topic)


@login_required
def delete_commento(request, pk):
    commento = get_object_or_404(Commento, pk=pk)
    data = dict()
    if request.method == 'POST':
        commento.delete()
        data['form_is_valid'] = True
    else:
        context = {'commento': commento}
        data['html_form'] = render_to_string(
            'forum/delete_commento.html',
            context,
            request=request
        )
    return JsonResponse(data)


@login_required
def add_topic(request):
    data = dict()

    if request.method == 'POST':
        form = TopicForm(request.POST)
        if form.is_valid():
            form_partial = form.save(commit=False)
            form_partial.gymuser_topic = request.user
            form_partial.save()
            form = form_partial
            data['form_is_valid'] = True
        else:
            data['form_is_valid'] = False
    else:
        form = TopicForm()

    context = {'form': form}
    data['html_form'] = render_to_string(
        'forum/crea_topic.html',
        context,
        request=request
    )
    return JsonResponse(data)


@login_required
def delete_topic(request, pk):
    topic = get_object_or_404(Topic_lezione, pk=pk)
    data = dict()

    if request.method == 'POST':
        topic.delete()
        data['form_is_valid'] = True
    else:
        context = {'topic': topic}
        data['html_form'] = render_to_string('forum/delete_topic.html', context, request=request)

    return JsonResponse(data)



