from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from django.urls import re_path
from django.contrib.auth import views as auth_views
from django.views.generic.base import RedirectView

from . import views

app_name = 'gestione_palestra'

urlpatterns = [
    path('homepage', views.homepage, name='homepage'),
    path('insegnanti', views.istruttori, name='insegnanti'),
    path('insegnanti/<int:id>', views.istruttore_info, name='insegnante_info'),
    path('contatti', views.contatti, name='contatti'),
    path('notizie/<int:id>', views.notizia_info, name='notizia_info'),
    path('notizie', views.notizie, name='notizie'),
    path('profile/<int:pk>', views.ShowProfile.as_view(), name='profile'),
    path('modifica/<int:pk>', views.ModifyUser.as_view(), name='modifica_user'),
    path('modificaPassword', views.PasswordChangeView, name='modica_password'),
    path('gallery', views.gallery, name='gallery'),
    path('logout', auth_views.LogoutView.as_view(), name='logout'),
    path('corsi', views.corsi, name='corsi'),
    path('corsi/<int:id>', views.corso_info, name='corso_info'),
    path('registrazione', views.SignUp.as_view(), name='registrazione'),
    path('login', auth_views.LoginView.as_view(), name='login'),
    path('check_username_unique', views.check_username_is_unique, name='check-username-unique'),
    re_path(r'^$', RedirectView.as_view(url='homepage', permanent=False), name='index'),
]
