from django.contrib import admin

from gestione_palestra import forms
from gestione_palestra.models import Corso, Notizia, Gallery, GymUser, InsegnanteCorso

admin.site.register(Corso)
admin.site.register(GymUser)
admin.site.register(Notizia)
admin.site.register(Gallery)


class InsegnanteCorsoAdmin(admin.ModelAdmin):
    form = forms.InsegnanteCorsoForm

    list_display = ('corso', 'insegnante')
    fields = ('corso', 'insegnante')


admin.site.register(InsegnanteCorso, InsegnanteCorsoAdmin)