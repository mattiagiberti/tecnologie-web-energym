from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse


# test view
from gestione_palestra.models import GymUser, Notizia, Corso, InsegnanteCorso
from lezioni.models import Lezione, Prenotazione


class TestViews(TestCase):
    def setUp(self):

        User = get_user_model()
        self.user = User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')

        self.corso = Corso.objects.create(nome="zumba")

        self.insegnante = GymUser.objects.create(
            email="trainer@trainer.com",
            first_name="personal",
            last_name="trainer",
            username="trainer",
            is_insegnante=True
        )

        self.insegnantecorso = InsegnanteCorso.objects.create(corso=self.corso, insegnante=self.insegnante)

        self.notizia = Notizia.objects.create(titolo="notizia di zumba")

        self.lezione = Lezione.objects.create(
            insegnantecorso=self.insegnantecorso,
            posti_disponibili_tot=2,  # 2 posti disponibili
            data='2222-12-05 16:00'
        )

    def test_homepage_notizie_consigliate_vuoto(self):
        """
        Test su view homepage, utente loggato non è iscritto a lezioni.
        Nessuna notizia consigliata
        """
        self.client.login(username='temporary', password='temporary')
        response = self.client.get(reverse('gestione_palestra:homepage'))
        self.assertContains(response, "Non ci sono notizie consigliate per te")
        self.assertEqual(response.status_code, 200)

    def test_homepage_notizie_consigliate(self):
        """
        Test su view homepage, utente loggato iscritto a lezioni
        La lezione a cui è iscritto (corso "zumba") è collegata alla notizia "zumba"
        Viene consigliata la notizia
        """
        Prenotazione.objects.create(user=self.user, lezione=self.lezione)
        self.client.login(username='temporary', password='temporary')
        response = self.client.get(reverse('gestione_palestra:homepage'))
        self.assertContains(response, "Notizie che potrebbero interessarti")
        self.assertEqual(response.status_code, 200)

    def test_istruttori(self):
        response = self.client.get(reverse('gestione_palestra:insegnanti'))
        self.assertEqual(response.status_code, 200)

    def test_corsi(self):
        response = self.client.get(reverse('gestione_palestra:corsi'))
        self.assertEqual(response.status_code, 200)

    def test_contatti(self):
        response = self.client.get(reverse('gestione_palestra:contatti'))
        self.assertEqual(response.status_code, 200)

    def test_gallery(self):
        response = self.client.get(reverse('gestione_palestra:gallery'))
        self.assertEqual(response.status_code, 200)
